hero.png
format: RGBA8888
filter: Linear,Linear
repeat: none
walk-e
  rotate: false
  xy: 0, 192
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
attack-sword-e
  rotate: false
  xy: 0, 0
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-s
  rotate: false
  xy: 0, 320
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
magic-w
  rotate: false
  xy: 192, 128
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
attack-sword-n
  rotate: false
  xy: 128, 0
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
attack-sword-w
  rotate: false
  xy: 128, 64
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-e
  rotate: false
  xy: 192, 192
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 3
walk-w
  rotate: false
  xy: 64, 384
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
walk-n
  rotate: false
  xy: 64, 256
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
walk-s
  rotate: false
  xy: 192, 320
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 3
magic-s
  rotate: false
  xy: 128, 128
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-e
  rotate: false
  xy: 128, 192
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 2
walk-w
  rotate: false
  xy: 0, 384
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-n
  rotate: false
  xy: 0, 256
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-s
  rotate: false
  xy: 128, 320
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 2
attack-sword-s
  rotate: false
  xy: 64, 64
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
magic-e
  rotate: false
  xy: 0, 128
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-w
  rotate: false
  xy: 192, 384
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 3
magic-n
  rotate: false
  xy: 64, 128
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
walk-n
  rotate: false
  xy: 192, 256
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 3
walk-e
  rotate: false
  xy: 64, 192
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
attack-sword-e
  rotate: false
  xy: 64, 0
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
walk-s
  rotate: false
  xy: 64, 320
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
attack-sword-n
  rotate: false
  xy: 192, 0
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
attack-sword-s
  rotate: false
  xy: 0, 64
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 0
attack-sword-w
  rotate: false
  xy: 192, 64
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 1
walk-w
  rotate: false
  xy: 128, 384
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 2
walk-n
  rotate: false
  xy: 128, 256
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: 2
