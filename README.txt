LEGEND OF ROGUE
An AHS Gaming Production
(c) 2013 Jami Couch
fbcouch 'at' gmail 'dot' com

Full source code available from:
	www.ahsgaming.com

Version 0.1 released 2013-04-04 for the One Game A Month (onegameamonth.org) challenge

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This project uses:

LibGDX
Copyright 2011 see LibGDX AUTHORS file
Licensed under Apache License, Version 2.0 (see above).

The font art assets used were created by Kenney, Inc. licensed as CC-SA.
	http://kenney.nl/

All other art assets are original creations by Jami Couch and are licensed as CC-BY-SA.

RUNNING THE GAME

This game was built with Java 7, so you may need to update your Java installation (www.java.com).
Otherwise, it should just run when you double-click the icon or right-click and run with Java runtime.
Alternatively, you may use the following command in a CLI:

	java -jar LegendOfRogue-0.1.java
	
PLAYING THE GAME

    Controls:
        Movement: UP, DOWN, LEFT, RIGHT Arrow Keys
        Combat: SPACE swings sword, LEFT SHIFT casts magic
        Inventory: E opens/closes inventory, SPACE uses item


