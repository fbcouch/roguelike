/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author jami
 *
 */
public class GameObject extends Actor {
	public static String LOG = "GameObject";
	
	protected final World world;
	
	Body physicsBody;
	TextureRegion image;
	Vector2 centerOffset = new Vector2();

	protected boolean isRemove = false;
	
	public GameObject(World world, Vector2 size) { 
		this.world = world;
		
		
		setSize(size.x, size.y);
		
		initPhysics();
	}
	
	public GameObject(World world, TextureRegion image) {
		this.world = world;
		this.image = image;
		setSize(image.getRegionWidth(), image.getRegionHeight());
		centerOffset = new Vector2(-1 * getWidth() * 0.5f, -1 * getHeight() * 0.5f);
		
		initPhysics();
		
	}
	
	protected void initPhysics() {
		// Dynamic Body
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		//bodyDef.position.set(100, 100);
		
		physicsBody = world.createBody(bodyDef);
		CircleShape dynamicCircle = new CircleShape();
		dynamicCircle.setRadius(((int)(getHeight() * 0.5f) - 1) * RogueLike.WORLD_TO_BOX);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = dynamicCircle;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 1;
		physicsBody.createFixture(fixtureDef).setUserData(this);
	}
	
	
	public void update(float delta) { }

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, float)
	 */
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		
		if (physicsBody != null) setRotation(MathUtils.radiansToDegrees * physicsBody.getAngle());
		if (physicsBody != null) setPosition(physicsBody.getPosition().x * RogueLike.BOX_TO_WORLD, physicsBody.getPosition().y * RogueLike.BOX_TO_WORLD);
		
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
		
		if (image != null) batch.draw(image, getX() + centerOffset.x, getY() + centerOffset.y, image.getU(), image.getV(), image.getRegionWidth(), image.getRegionHeight(), 1, 1, getRotation());
		
		
	}
	
	public boolean isPhysicsActive() {
		return (physicsBody != null ? physicsBody.isActive() : false);
	}
	
	public void setPhysicsActive(boolean active) {
		if (physicsBody != null) 
			physicsBody.setActive(active);
	}
	
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#setPosition(float, float)
	 */
	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		if (physicsBody != null) physicsBody.setTransform(x * RogueLike.WORLD_TO_BOX, y * RogueLike.WORLD_TO_BOX, physicsBody.getAngle());
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#setRotation(float)
	 */
	@Override
	public void setRotation(float degrees) {
		super.setRotation(degrees);
		if (physicsBody != null) physicsBody.setTransform(physicsBody.getTransform().getPosition(), degrees * MathUtils.degreesToRadians);
		
	}

	public Vector2 getLinearVelocity() {
		if (physicsBody != null) 
			return physicsBody.getLinearVelocity();
		return new Vector2();
	}
	
	public void setLinearVelocity(Vector2 vel) {
		if (physicsBody != null) physicsBody.setLinearVelocity(vel);
	}
	
	public void setLinearVelocity(float vx, float vy) {
		if (physicsBody != null) physicsBody.setLinearVelocity(vx * RogueLike.WORLD_TO_BOX, vy * RogueLike.WORLD_TO_BOX);
	}
	
	public boolean shouldCollide(GameObject other) {
		return true;
	}
	
	public void beginContact(GameObject other) {
		
	}
	
	public void endContact(GameObject other) {
		
	}
	
	public void destroy() {
		remove();
		if (physicsBody != null) {
			world.destroyBody(physicsBody);
			physicsBody = null;
		}
	}

	/**
	 * @return the isRemove
	 */
	public boolean isRemove() {
		return isRemove;
	}

	/**
	 * @param isRemove the isRemove to set
	 */
	public void setRemove(boolean isRemove) {
		this.isRemove = isRemove;
	}
	
	public Vector2 getCenter() {
		return centerOffset;
	}
}
