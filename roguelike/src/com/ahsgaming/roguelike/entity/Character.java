/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.TextureManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class Character extends Entity {
	public static String LOG = "Character";
	
	// directions
	public static final int SOUTH = 0, EAST = 1, NORTH = 2, WEST = 3;
	
	int facing = SOUTH;
	
	Animation currentAnim = null;
	Animation currentEquipAnim = null;
	float animTime = 0;
	
	Array<DamageBox> damageBoxes = new Array<DamageBox>();
	
	Array<Item> inventory = new Array<Item>();
	
	int maxHP, curHP;
	int attackDamage;
	float attackSpeed, attackTimer = 0;
	int abilityPower;
	float castSpeed, castTimer = 0;
	
	int armor;
	
	float moveSpeed;
	
	// STATES
	boolean isWalking = false, isAttacking = false, isCasting = false;
	
	//-------------
	// Equipment
	//-------------
	Item eqSword, eqRune, eqArmor;
	
	//-------------
	// Stats
	//-------------
	Array<DamageToken> damaged = new Array<DamageToken>();
	
	Array<DamageToken> damagedBy = new Array<DamageToken>();
	
	
	/**
	 * @param world
	 * @param spriteFromAtlas
	 */
	public Character(World world, TextureRegion region) {
		super(world, new EntityProto(), region);
	}

	/**
	 * @param world
	 * @param proto
	 */
	public Character(World world, EntityProto proto) {
		super(world, proto);
	}

	/**
	 * @param world
	 * @param proto
	 * @param spriteFromAtlas
	 */
	public Character(World world, EntityProto proto, Sprite region) {
		super(world, proto, region);
	}

	/**
	 * @param world
	 * @param atlas
	 */
	public Character(World world, String atlas) {
		super(world, new EntityProto(), TextureManager.getSpriteFromAtlas(atlas, "walk", 0));
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#update(float, com.ahsgaming.roguelike.entity.PlayerCharacter)
	 */
	@Override
	public void update(float delta, PlayerCharacter pc) {
		super.update(delta, pc);
		
		if (attackTimer > 0) {
			attackTimer -= delta;
		} else {
			attackTimer = 0;
			isAttacking = false;
		}
		
		if (castTimer > 0) {
			castTimer -= delta;
		} else {
			castTimer = 0;
			isCasting = false;
		}
		
		Array<DamageBox> toRemove = new Array<DamageBox>();
		for (DamageBox db: damageBoxes) {
			db.update(delta);
			if (db.isRemove) toRemove.add(db);
		}
		damageBoxes.removeAll(toRemove, true);
		
		if (currentAnim != null || currentEquipAnim != null) {
			animTime += delta;
			if (currentAnim != null)
				this.image = currentAnim.getKeyFrame(animTime);
			
			if ((currentAnim != null && currentAnim.isAnimationFinished(animTime))
					|| (currentEquipAnim != null && currentEquipAnim.isAnimationFinished(animTime))) {
				currentAnim = null;
				currentEquipAnim = null;
				animTime = 0;
				
				if (isCasting) endCast();
				
				if (isAttacking) isAttacking = false;
				if (isWalking) isWalking = false;
				if (isCasting) isCasting = false;
			}
		} else {
			this.image = TextureManager.getSpriteFromAtlas(proto.image, "walk" + directionToString(facing), 0);
			if (isWalking) isWalking = false;
			if (isAttacking) isAttacking = false;
			if (isCasting) isCasting = false;
		}
		
		if (this.curHP <= 0) {
			this.isRemove = true;
			
			if (this.getLastDamagedBy() != null && this.getLastDamagedBy().character.equals(pc)) {
				pc.gainXP(getXPValue());
			}
			
		}
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, float)
	 */
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if (currentEquipAnim != null && isCasting) {
			TextureRegion frame = currentEquipAnim.getKeyFrame(animTime);
			if (frame != null) batch.draw(frame, 
					getX() + centerOffset.x + ((getWidth() - frame.getRegionWidth()) * 0.5f), 
					getY() + centerOffset.y + ((getHeight() - frame.getRegionHeight()) * 0.5f),
					frame.getU(), frame.getV(), frame.getRegionWidth(), frame.getRegionHeight(),
					1, 1, getRotation());
		}
		
		for (DamageBox d: damageBoxes) {
			d.draw(batch, parentAlpha);
		}
	}	
	
	/*---------------------------------------------------------------------------
	 * Combat
	 *---------------------------------------------------------------------------*/

	public boolean canAttack() {
		return (!isAttacking && attackTimer <= 0 && !isCasting);
	}
	
	public void attack(Character other) {
		if (canAttack()) {
			other.takeDamage(getAttackDamage(), getAttackDamageType());
			attackTimer = getAttackSpeed();
			isAttacking = true;
			
			currentAnim = Character.getAnimation(proto.image, "attack-sword", facing, getAttackAnimSpeed(), 0);
			animTime = 0;
			
		}
	}
	
	/**
	 * Unlike MOBs, players attack with weapons/hitboxes, etc
	 */
	public void attack() {
		if (canAttack()) {
			Vector2 pos = new Vector2();
			switch(facing) {
			case SOUTH:
				pos.y -= getHeight() - 8 - (eqSword == null ? 16 : 0);
				break;
			case EAST:
				pos.x += getWidth() - 8 - (eqSword == null ? 16 : 0);
				break;
			case NORTH:
				pos.y += getHeight() - 8 - (eqSword == null ? 16 : 0);
				break;
			case WEST:
				pos.x -= getWidth() - 8 - (eqSword == null ? 16 : 0);
				break;
			}
			
			currentAnim = Character.getAnimation(proto.image, "attack-sword", facing, getAttackAnimSpeed(), 0);
			animTime = 0;
			
			damageBoxes.add(new DamageBox(world, this, 
					new Vector2(50 + (facing == SOUTH || facing == NORTH ? 64 : 0) + (eqSword == null ? -25 : 0), 50 + (facing == EAST || facing == WEST ? 64 : 0) + (eqSword == null ? -25 : 0)), 
					getAttackDamage(), getAttackDamageType(), currentAnim.animationDuration, pos, new Vector2()));
			
			this.attackTimer = getAttackSpeed();
			
			isAttacking = true;
			
			
		}
	}
	
	public boolean canCast() {
		return (!isCasting && !isAttacking && castTimer <= 0 && eqRune != null);
	}
	
	public void cast() {
		if (canCast()) {
			
			currentAnim = Character.getAnimation(proto.image, "magic", facing, getMagicAnimSpeed(), Animation.NORMAL);
			currentEquipAnim = Character.getAnimation("items.txt", eqRune.getImage() + "-cast", facing, getMagicAnimSpeed(), Animation.NORMAL);
			
			animTime = 0;
			castTimer = getCastSpeed();
			isCasting = true;
			
		}
	}
	
	public void endCast() {
		
		float speed = 1;
		if (eqRune.getProperties().containsKey("speed")) {
			speed = Float.parseFloat(eqRune.getProperties().get("speed").toString());
		}
		
		float duration = 0.5f;
		if (eqRune.getProperties().containsKey("duration")) {
			duration = Float.parseFloat(eqRune.getProperties().get("duration").toString());
		}
		
		
		TextureRegion proj = TextureManager.getSpriteFromAtlas("items.txt", eqRune.getImage() + "-projectile", -1);
		damageBoxes.add(new CircleDamageBox(world, this, proj, 
				getAbilityPower(), getCastDamageType(), duration, new Vector2(), 
				new Vector2(speed * (facing == NORTH || facing == SOUTH? 0: (facing == EAST ? 1 : -1)), speed * (facing == EAST || facing == WEST ? 0 : (facing == NORTH ? 1 : -1))), true));
		
		
	}
	
	public int getDamage() {
		return attackDamage;
	}
	
	public int takeDamage(int damage, String damageType) {
		damage -= (armor + damage * getResist(damageType));
		
		if (damage > 0) { 
			curHP -= damage;
			this.addAction(Actions.repeat(3, Actions.sequence(Actions.color(new Color(0.5f, 0, 0, 1), 0.05f), Actions.color(new Color(1, 1, 1, 1), 0.05f))));
			return damage;
		}
		return 0;
	}
	
	public String getAttackDamageType() {
		if (this.eqSword != null) return eqSword.getDamageType();
		if (this.properties.containsKey("damagetype"))
			return properties.get("damagetype").toString();
		return "physical";
	}
	
	public String getCastDamageType() {
		if (this.eqRune != null) return eqRune.getDamageType();
		return "magic";
	}
	
	@SuppressWarnings("unchecked")
	public float getResist(String damageType) {
		if (properties.containsKey("resists")) {
			ObjectMap<String, Object> om = (ObjectMap<String, Object>)properties.get("resists");
			if (om.containsKey(damageType)) {
				return Float.parseFloat(om.get(damageType).toString());
			}
		}
		return 0;
	}
	
	/*---------------------------------------------------------------------------
	 * Walk
	 *---------------------------------------------------------------------------*/
	
	public void walk() {
		if (!isAttacking && !isCasting) {
			isWalking = true;
			
			currentAnim = Character.getAnimation(proto.image, "walk", facing, getWalkAnimSpeed(), 0);
			animTime = 0;
			
		}
	}
	
	/*---------------------------------------------------------------------------
	 * Stats
	 *---------------------------------------------------------------------------*/
	
	public int getAttackDamage() {
		int total = this.attackDamage;
		
		if (eqSword != null)
			total += eqSword.getAttackDamage();
		
		if (eqRune != null)
			total += eqRune.getAttackDamage();
		
		if (eqArmor != null)
			total += eqArmor.getAttackDamage();
		
		return total;
	}
	
	public float getAttackSpeed() {
		float total = this.attackSpeed;
		
		if (eqSword != null)
			total += eqSword.getAttackSpeed();
		
		if (eqRune != null)
			total += eqRune.getAttackSpeed();
		
		if (eqArmor != null)
			total += eqArmor.getAttackSpeed();
		
		return total;
	}
	
	public int getAbilityPower() {
		int total = this.abilityPower;
		
		if (eqSword != null)
			total += eqSword.getAbilityPower();
		
		if (eqRune != null)
			total += eqRune.getAbilityPower();
		
		if (eqArmor != null)
			total += eqArmor.getAbilityPower();
		
		return total;
	}
	
	public float getCastSpeed() {
		float total = this.castSpeed;
		
		if (eqSword != null)
			total += eqSword.getCastSpeed();
		
		if (eqRune != null)
			total += eqRune.getCastSpeed();
		
		if (eqArmor != null)
			total += eqArmor.getCastSpeed();
		
		return total;
	}
	
	public int getArmor() {
		int total = this.armor;
		
		if (eqSword != null)
			total += eqSword.getArmor();
		
		if (eqRune != null)
			total += eqRune.getArmor();
		
		if (eqArmor != null)
			total += eqArmor.getArmor();
		
		return total;
	}
	
	public int getXPValue() {
		return maxHP;
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#parseProperties()
	 */
	@Override
	public void parseProperties() {
		super.parseProperties();
		
		if (properties.containsKey("abilitypower"))
			abilityPower = (int)Float.parseFloat(properties.get("abilitypower").toString());
		
		if (properties.containsKey("attackdamage"))
			attackDamage = (int)Float.parseFloat(properties.get("attackdamage").toString());
		
		if (properties.containsKey("attackspeed"))
			attackSpeed = Float.parseFloat(properties.get("attackspeed").toString());
		
		if (properties.containsKey("armor"))
			armor = (int)Float.parseFloat(properties.get("armor").toString());
		
		if (properties.containsKey("castspeed"))
			castSpeed = Float.parseFloat(properties.get("castspeed").toString());
		
		if (properties.containsKey("curhp"))
			curHP = (int)Float.parseFloat(properties.get("curhp").toString());
		
		if (properties.containsKey("maxhp"))
			maxHP = (int)Float.parseFloat(properties.get("maxhp").toString());
		
		if (properties.containsKey("movespeed"))
			moveSpeed = Float.parseFloat(properties.get("movespeed").toString());
		
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#updateProperties()
	 */
	@Override
	public void updateProperties() {
		super.updateProperties();
		
		properties.put("abilitypower", abilityPower);
		properties.put("attackdamage", attackDamage);
		properties.put("attackspeed", attackSpeed);
		properties.put("armor", armor);
		properties.put("castspeed", castSpeed);
		properties.put("curhp", curHP);
		properties.put("maxhp", maxHP);
		properties.put("movespeed", moveSpeed);
		
			
	}

	/**
	 * @return
	 */
	public Array<Item> getInventory() {
		return inventory;
	}
	
	public int getHP() {
		return curHP;
	}
	
	public int getMaxHP() {
		return maxHP;
	}
	
	/**
	 * @return the eqSword
	 */
	public Item getEqSword() {
		return eqSword;
	}

	/**
	 * @param eqSword the eqSword to set
	 */
	public void setEqSword(Item eqSword) {
		this.eqSword = eqSword;
	}

	/**
	 * @return the eqRune
	 */
	public Item getEqRune() {
		return eqRune;
	}

	/**
	 * @param eqRune the eqRune to set
	 */
	public void setEqRune(Item eqRune) {
		this.eqRune = eqRune;
	}

	/**
	 * @return the eqArmor
	 */
	public Item getEqArmor() {
		return eqArmor;
	}

	/**
	 * @param eqArmor the eqArmor to set
	 */
	public void setEqArmor(Item eqArmor) {
		this.eqArmor = eqArmor;
	}
	
	public void addDamaged(Character other, int amount, String type) {
		Gdx.app.log(LOG, String.format("%s damaged %s for %d %s", this.name, other.name, amount, type));
		damaged.add(new DamageToken(other, amount, type));
	}
	
	public void addDamagedBy(Character other, int amount, String type) {
		damagedBy.add(new DamageToken(other, amount, type));
	}
	
	public DamageToken getLastDamagedBy() {
		return (damagedBy.size > 0 ? damagedBy.peek() : null);
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#destroy()
	 */
	@Override
	public void destroy() {
		super.destroy();
		
		for (DamageBox d: damageBoxes) {
			d.destroy();
		}
	}

	//-------------------------------------------------------------------------
	// Static methods
	//-------------------------------------------------------------------------
	public static String directionToString(int direction) {
		switch(direction) {
		case SOUTH:
			return "-s";
		case NORTH:
			return "-n";
		case WEST:
			return "-w";
		case EAST:
			return "-e";
		}
		return "";
	}
	
	public static Animation getAnimation(String image, String animName, int facing, float animSpeed, int playType) {
		Array<Sprite> sprites = TextureManager.getSpritesFromAtlas(image, animName + Character.directionToString(facing));
		if (sprites.size == 0)
			sprites = TextureManager.getSpritesFromAtlas(image, animName);
		if (sprites.size != 0)
			return new Animation(animSpeed, sprites, playType);
		
		return null;
	}
	
	public static Animation getAnimation(String image, String animName, int facing, int playType) {
		return getAnimation(image, animName, facing, 0.5f, playType);
	}
	
	//-------------------------------------------------------------------------
	// Static classes
	//-------------------------------------------------------------------------
	
	public static class DamageToken {
		public Character character;
		public int amount;
		public String type;
		
		public DamageToken() {
			character = null;
			amount = 0;
			type = "physical";
		}
		
		public DamageToken(Character character, int amount, String type) {
			this.character = character;
			this.amount = amount;
			this.type = type;
		}
	}
}
