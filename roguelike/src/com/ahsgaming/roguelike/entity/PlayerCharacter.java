/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class PlayerCharacter extends Character {
	public static String LOG = "PlayerCharacter";
	
	ObjectMap<String, Integer> actionKeys = new ObjectMap<String, Integer>();
	
	Monster target;
	
	Array<String> itemsToAdd = new Array<String>();
	
	int xp;
	int level;
	
	
	/**
	 * @param world
	 * @param image
	 */
	public PlayerCharacter(World world, EntityProto entityProto) {
		super(world, entityProto, TextureManager.getSpriteFromAtlas(entityProto.image, "walk-s", 0));
		
		
		
		actionKeys = RogueLike.actionKeys;
		
		for (Fixture f: physicsBody.getFixtureList()) {
			f.setRestitution(0);
		}
	}
	
	/**
	 * @param world
	 * @param entityProto
	 * @param inventory
	 */
	public PlayerCharacter(World world, EntityProto entityProto,
			Array<EntityProto> inventory) {
		this(world, entityProto);
		
		for (EntityProto ep: inventory) {
			if (!ep.id.startsWith("ITEM")) continue;
			
			EntityProto proto = EntityManager.get(ep.id);
			ep.title = proto.title;
			ep.desc = proto.desc;
			
			this.inventory.add(new Item(world, ep, true));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#update(float, com.ahsgaming.roguelike.entity.PlayerCharacter)
	 */
	@Override
	public void update(float delta, PlayerCharacter pc) {
		super.update(delta, pc);
		
		for (String id: itemsToAdd) {
			Item ci = new Item(world, EntityManager.get(id), true);
			inventory.add(ci);
		}
		itemsToAdd.clear();
		
		if (xp > getXPforLevel(level)) {
			levelUp();
		}
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, float)
	 */
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (currentEquipAnim != null && isAttacking) {
			Color color = getColor();
			batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
			TextureRegion frame = currentEquipAnim.getKeyFrame(animTime);
			int drawX = (int) (getX() + centerOffset.x);
			int drawY = (int) (getY() + centerOffset.y);
			switch(facing) {
			case NORTH:
				drawX -= (int)((frame.getRegionWidth() - getWidth()) * 0.5f);
				//drawY -= 8;
				break;
			case SOUTH:
				drawX -= (int)((frame.getRegionWidth() - getWidth()) * 0.5f);
				drawY -= (int)(frame.getRegionHeight() * 0.5f) - 8;
				break;
			case EAST:
				//drawX += 8;
				drawY -= (int)((frame.getRegionHeight() - getHeight()) * 0.5f);
				break;
			case WEST:
				drawX -= (int)((frame.getRegionWidth()) * 0.5f) - 8;
				drawY -= (int)((frame.getRegionHeight() - getHeight()) * 0.5f);
				break;
			}
			if (frame != null) batch.draw(frame, drawX, drawY, frame.getU(), frame.getV(), frame.getRegionWidth(), frame.getRegionHeight(), 1, 1, getRotation());
		}
		super.draw(batch, parentAlpha);
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#attack()
	 */
	@Override
	public void attack() {
		if (canAttack()) {
			super.attack();
			if (eqSword != null) {
				Array<Sprite> spe = TextureManager.getSpritesFromAtlas("items.txt" , eqSword.getImage() + "-anim" + directionToString(facing));
				if (spe.size > 0) {
					currentEquipAnim = new Animation(getAttackAnimSpeed(), spe);
				}
			}
		}
	}

	public void processInput() {
		setLinearVelocity(0, 0);
		boolean walk = false;
		int saveFacing = facing;
		if (Gdx.input.isKeyPressed(actionKeys.get("move-up"))) {
			setLinearVelocity(getLinearVelocity().x, 1);
			if (!isAttacking) {
				facing = NORTH;
				walk = true;
			}
		} else if (Gdx.input.isKeyPressed(actionKeys.get("move-down"))) {
			setLinearVelocity(getLinearVelocity().x, -1);
			if (!isAttacking) {
				facing = SOUTH;
				walk = true;
			}
		}
		
		if (Gdx.input.isKeyPressed(actionKeys.get("move-left"))) {
			setLinearVelocity(-1, getLinearVelocity().y);
			if (!isAttacking) {
				facing = WEST;
				walk = true;
			}
		} else if (Gdx.input.isKeyPressed(actionKeys.get("move-right"))) {
			setLinearVelocity(1, getLinearVelocity().y);
			if (!isAttacking) {
				facing = EAST;
				walk = true;
			}
		}
		
		if (Gdx.input.isKeyPressed(actionKeys.get("attack-1"))) {
			attack();
		}
		
		if (Gdx.input.isKeyPressed(actionKeys.get("attack-2"))) {
			cast();
		}
		
		if (!isAttacking && !isCasting && walk) {
			if (!isWalking || facing != saveFacing) walk();
		}
		
		setLinearVelocity(getLinearVelocity().nor().mul(moveSpeed));
	}

	public void useItem(Item item) {
		int curUses = 1;
		if (item.getProperties().containsKey("uses")) {
			curUses = (int)Float.parseFloat(item.getProperties().get("uses").toString());
		}
		
		curUses -= 1;
		if (curUses <= 0) {
			inventory.removeValue(item, true);
		}
		
		if (item.getProperties().containsKey("curhp")) {
			curHP += (int)Float.parseFloat(item.getProperties().get("curhp").toString());
			if (curHP > maxHP) curHP = maxHP;
		}
		
		if (item.getProperties().containsKey("maxhp")) {
			maxHP += (int)Float.parseFloat(item.getProperties().get("maxhp").toString());
		}
		
		updateProperties();
	}
	
	public void removeItem(Item item) {
		inventory.removeValue(item, true);
	}


	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#beginContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void beginContact(GameObject other) {
		super.beginContact(other);
		
		if (other instanceof Monster) {
			target = (Monster)other;
		}
		
		if (other instanceof Item) {
			Item i = (Item)other;
			
			if (i.isContainer() && i.isStatic()) {
				itemsToAdd.addAll(i.popContainedItems());
			} else {
				inventory.add(i);
				other.setRemove(true);
			}
			
			if (i.getType().equals("sword") && eqSword == null) {
				eqSword = i;
			}
			
			if (i.getType().equals("rune") && eqRune == null) {
				eqRune = i;
			}
			
			if (i.getType().equals("armor") && eqArmor == null) {
				eqArmor = i;
			}
		}
		
		if (other instanceof Door) {
			
			Door d = (Door)other;
			if (d.locked) {
				
				Item key = null;
				for (Item i: inventory) {
					if (i.type.equals("key") && d.canKeyUnlock(i)) {
						key = i;
					}
				}
				if (key != null) {
					
					d.locked = false;
					d.updateProperties();
					d.setRemove(true);
					inventory.removeValue(key, true);
					updateProperties();
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#endContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void endContact(GameObject other) {
		super.endContact(other);
		
		target = null;
	}

	
	public int getXPforLevel(int level) {
		return (level * level) * 100;
	}
	
	public void levelUp() {
		level += 1;
		Gdx.app.log(LOG, String.format("Level up to %d", level));
		int hpgain = level * 10;
		this.curHP += hpgain;
		this.maxHP += hpgain;
		
		this.attackDamage += level;
		this.abilityPower += level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getXP() {
		return xp;
	}
	
	public void setXP(int xp) {
		this.xp = xp;
	}
	
	public void gainXP(int xp) {
		Gdx.app.log(LOG, String.format("XP Gained: %d", xp));
		this.xp += xp;
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#parseProperties()
	 */
	@Override
	public void parseProperties() {
		super.parseProperties();
		
		if (properties.containsKey("level"))
			level = (int)Float.parseFloat(properties.get("level").toString());
		
		if (properties.containsKey("xp"))
			xp = (int)Float.parseFloat(properties.get("xp").toString());
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#updateProperties()
	 */
	@Override
	public void updateProperties() {
		super.updateProperties();
		
		properties.put("level", level);
		properties.put("xp", xp);
	}
	
	
}
