/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * @author jami
 *
 */
public class DamageBox extends GameObject {
	public static String LOG = "DamageBox";
	
	int damage = 0;
	float duration = 0;
	Character owner;
	Vector2 speed = new Vector2();
	Vector2 offset = new Vector2();
	
	Array<Character> damaged = new Array<Character>();
	
	boolean destroyOnHit = false;
	String damageType = "physical";
	
	/**
	 * @param world
	 * @param image
	 */
	public DamageBox(World world, Character owner, Vector2 size, int damage, String damageType, float duration, Vector2 offset, Vector2 speed) {
		super(world, size);
		
		this.damage = damage;
		this.damageType = damageType;
		this.duration = duration;
		this.owner = owner;
		this.speed = speed;
		this.offset = offset;
		
		this.setPosition(owner.getX() + offset.x, owner.getY() + offset.y);
		this.setLinearVelocity(speed);
	}
	
	public DamageBox(World world, Character owner, Vector2 size, int damage, String damageType, float duration, Vector2 offset, Vector2 speed, boolean destroyOnHit) {
		this (world, owner, size, damage, damageType, duration, offset, speed);
		this.destroyOnHit = destroyOnHit;
	}
	/**
	 * @param world
	 * @param image
	 */
	public DamageBox(World world, Character owner, TextureRegion image, int damage, String damageType, float duration, Vector2 offset, Vector2 speed) {
		super(world, image);
		
		this.damage = damage;
		this.damageType = damageType;
		this.duration = duration;
		this.owner = owner;
		this.speed = speed;
		this.offset = offset;
		
		this.setPosition(owner.getX() + offset.x, owner.getY() + offset.y);
		this.setLinearVelocity(speed);
	}
	
	public DamageBox(World world, Character owner, TextureRegion image, int damage, String damageType, float duration, Vector2 offset, Vector2 speed, boolean destroyOnHit) {
		this(world, owner, image, damage, damageType, duration, offset, speed);
		
		this.destroyOnHit = destroyOnHit;
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#initPhysics()
	 */
	@Override
	protected void initPhysics() {
		// Dynamic Body
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		
		physicsBody = world.createBody(bodyDef);
		PolygonShape dynamicBox = new PolygonShape();
		dynamicBox.setAsBox((getWidth() * 0.5f - 2) * RogueLike.WORLD_TO_BOX, (getHeight() * 0.5f - 2) * RogueLike.WORLD_TO_BOX);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = dynamicBox;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0;
		physicsBody.createFixture(fixtureDef).setUserData(this);
	}



	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#update(float)
	 */
	@Override
	public void update(float delta) {
		super.update(delta);
		
		this.duration -= delta;
		if (duration < 0) {
			this.remove();
			this.destroy();
			this.isRemove = true;
		}
		
		if (this.speed.x == 0 && this.speed.y == 0) {
			// this guy is fixed in place
			this.setPosition(owner.getX() + offset.x, owner.getY() + offset.y);
		
			this.setLinearVelocity(speed);
		} else {
			if (this.getLinearVelocity().x == 0 && this.getLinearVelocity().y == 0) {
				this.remove();
				this.destroy();
				this.isRemove = true;
			}
		}
		
		this.setRotation(0);
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#shouldCollide(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public boolean shouldCollide(GameObject other) {
		
		if (super.shouldCollide(other)) {
			if (other instanceof Character) {
				if (other == owner || (owner instanceof Monster && other instanceof Monster)) {
					
					return false;
				}
				if (damaged.contains((Character)other, true)) return false;
				return true;
			} else if (other instanceof Door) {
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#beginContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void beginContact(GameObject other) {
		super.beginContact(other);
		
		if (other instanceof Character && other != owner && !damaged.contains((Character)other, true)) {
			int dmgTaken = ((Character)other).takeDamage(damage, damageType);
			damaged.add((Character)other);
			owner.addDamaged((Character)other, dmgTaken, damageType);
			((Character)other).addDamagedBy(owner, dmgTaken, damageType);
		}
		
		if (destroyOnHit && !(other instanceof Character && other == owner)) {
			this.isRemove = true;
			this.duration = 0;
		}
	}

	public boolean isOwner(GameObject obj) {
		return (obj instanceof Character && (Character)obj == owner);
	}

	/**
	 * 
	 */
	public void mapCollide() {
		if (destroyOnHit) {
		//	this.isRemove = true;
		//	this.duration = 0;
		}
	}
}
