/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * @author jami
 *
 */
public class Portal extends Entity {
	public static String LOG = "Portal";
	
	String mapDestination = "";
	int destX = 0;
	int destY = 0;
	
	/**
	 * @param world
	 * @param image
	 */
	public Portal(World world, EntityProto proto) {
		super(world, proto, TextureManager.getSpriteFromAtlas("items.txt", proto.image, -1));
		
		parseProperties();
		
		centerOffset.set(0, 0);
	}
	
	@Override
	protected void initPhysics() {
		BodyDef collideDef = new BodyDef();
		
		physicsBody = world.createBody(collideDef);
		PolygonShape tileBox = new PolygonShape();
		//EdgeShape tileBox = new EdgeShape();
		Vector2[] vertices = new Vector2[4];
		vertices[0] = (new Vector2(0, 0).mul(RogueLike.WORLD_TO_BOX));
		vertices[1] = (new Vector2(getWidth(), 0).mul(RogueLike.WORLD_TO_BOX));
		vertices[2] = (new Vector2(getWidth(), getHeight()).mul(RogueLike.WORLD_TO_BOX));
		vertices[3] = (new Vector2(0, getHeight()).mul(RogueLike.WORLD_TO_BOX));
		
		tileBox.set(vertices);
		physicsBody.createFixture(tileBox, 0.0f).setUserData(this);
	}

	@Override
	public void parseProperties() {
		if (properties.containsKey("dest-map"))
			mapDestination = properties.get("dest-map").toString();
		
		if (properties.containsKey("x"))
			destX = (int)Float.parseFloat(properties.get("x").toString());
		
		if (properties.containsKey("y"))
			destY = (int)Float.parseFloat(properties.get("y").toString());
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#updateProperties()
	 */
	@Override
	public void updateProperties() {
		super.updateProperties();
		properties.put("dest-map", mapDestination);
		properties.put("x", destX);
		properties.put("y", destY);
	}

	public String getMapDestination() {
		return mapDestination;
	}
	
	public int getDestX() {
		return destX;
	}
	
	public int getDestY() {
		return destY;
	}
}
