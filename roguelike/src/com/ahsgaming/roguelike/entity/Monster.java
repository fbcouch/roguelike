/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.TextureManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * @author jami
 *
 */
public class Monster extends Character {
	public static String LOG = "Monster";
	public static final int MAX_SIM_DIST_SQ = 1000*1000;
	
	float timeToChangeDir = 0.0f;
	Vector2 direction;
	
	Character target;
	
	String aiType = "melee";
	
	/**
	 * @param world
	 * @param proto
	 */
	public Monster(World world, EntityProto proto) {
		super(world, proto, (Sprite) Character.getAnimation(proto.image, "walk", 0, 0).getKeyFrame(0));
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#update(com.ahsgaming.roguelike.entity.PlayerCharacter)
	 */
	@Override
	public void update(float delta, PlayerCharacter pc) {
		super.update(delta, pc);
		
		if (Math.pow(pc.getX() - getX(), 2) + Math.pow(pc.getY() - getY(), 2) > MAX_SIM_DIST_SQ) return;
		
		timeToChangeDir -= delta;
		if (timeToChangeDir <= 0) {
			timeToChangeDir = 3;
			direction = new Vector2(moveSpeed, 0);
			direction.setAngle(MathUtils.random(0,7) * 45);
			this.setLinearVelocity(direction);
		}
		
		if (currentAnim == null) {
			currentAnim = Character.getAnimation(proto.image, "walk", facing, getWalkAnimSpeed(), Animation.LOOP);
		}
		
		Vector2 dirToPlayer = new Vector2(pc.getX(), pc.getY()).sub(new Vector2(getX(), getY()));
		
		if (Math.abs(dirToPlayer.x) >= Math.abs(dirToPlayer.y)) {
			facing = (dirToPlayer.x > 0 ? EAST : WEST);
		} else {
			facing = (dirToPlayer.y > 0 ? NORTH : SOUTH);
		}
		
		if (aiType.equals("caster")) {
			
			if (canCast()) cast();
			
		} else if(aiType.equals("penyo")) {
			
			if (target != null && canAttack()) attack();
			
			if (canCast()) cast();
			
			// when changing direction, sometimes charge at the player
			if (timeToChangeDir == 3 && MathUtils.random(10) > 9) {
				direction = new Vector2(moveSpeed, 0);
				direction.setAngle(dirToPlayer.angle());
				this.setLinearVelocity(direction);
			} else if (timeToChangeDir == 3) {
				this.setLinearVelocity(this.getLinearVelocity().mul(0.2f));
			}
		
		} else {
			if (target != null && canAttack()) attack(target);
		}
		
		
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#beginContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void beginContact(GameObject other) {
		super.beginContact(other);
		
		if (other instanceof PlayerCharacter) {
			target = (Character)other;
		}
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#endContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void endContact(GameObject other) {
		super.endContact(other);
		
		if (other == target) {
			target = null;
		}
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#parseProperties()
	 */
	@Override
	public void parseProperties() {
		super.parseProperties();
		
		if (properties.containsKey("ai"))
			aiType = properties.get("ai").toString();
		
		if (properties.containsKey("rune"))
			eqRune = new Item(world, EntityManager.get(properties.get("rune").toString()), true);
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Character#updateProperties()
	 */
	@Override
	public void updateProperties() {
		super.updateProperties();
		
		properties.put("ai", aiType);
		if (eqRune != null) { 
			properties.put("rune", eqRune.getProto().id);
		} else {
			if (properties.containsKey("rune")) properties.remove("rune");
		}
				
	}
	
	
	
	
}
