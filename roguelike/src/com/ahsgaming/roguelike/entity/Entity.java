/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.TextureManager;
import com.ahsgaming.roguelike.Utils;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class Entity extends GameObject {
	public static String LOG = "Entity";
	
	public static final float DEFAULT_WALK_ANIM_SPEED = 0.2f;
	public static final float DEFAULT_ATTACK_ANIM_SPEED = 0.1f;
	public static final float DEFAULT_MAGIC_ANIM_SPEED = 0.5f;
	
	String id = "";
	String type = "";
	String name = "entity";
	String sImage = "";
	ObjectMap<String, Object> properties = new ObjectMap<String, Object>();
	
	final EntityProto proto;
	
	/**
	 * @param world
	 * @param sImage
	 */
	public Entity(World world, EntityProto proto) {
		super(world, TextureManager.getTexture(proto.image));
		
		this.proto = proto;
		id = proto.id;
		type = proto.type;
		sImage = proto.image;
		properties.putAll(proto.properties);
		parseProperties();
	}
	
	public Entity(World world, EntityProto proto, TextureRegion texture) {
		super(world, texture);
		
		this.proto = proto;
		this.id = proto.id;
		type = proto.type;
		sImage = proto.image;
		properties.putAll(proto.properties);
		parseProperties();
	}
	
	public void update(float delta, PlayerCharacter pc) {
		
	}
	
	@Override
	public String toString() {
		updateProperties();
		
		String json = "    {\n";
		
		json += "      " + Utils.toJsonProperty("name", "\"" + name + "\"");
		json += "      " + Utils.toJsonProperty("type", id);
		json += "      " + Utils.toJsonProperty("image", sImage);
		json += "      " + Utils.toJsonProperty("properties", properties);
		json += "      " + Utils.toJsonProperty("x", MathUtils.round(getX()));
		json += "      " + Utils.toJsonProperty("y", MathUtils.round(getY()));
		json += "      " + Utils.toJsonProperty("width", getWidth());
		json += "      " + Utils.toJsonProperty("height", getHeight());
		
		json += "    }";
		return json;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return sImage;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.sImage = image;
	}

	/**
	 * @return the properties
	 */
	public ObjectMap<String, Object> getProperties() {
		updateProperties();
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(ObjectMap<String, Object> properties) {
		this.properties.putAll(properties);
		parseProperties();
	}

	/**
	 * @return the proto
	 */
	public EntityProto getProto() {
		return proto;
	}
	
	public void parseProperties() { }
	public void updateProperties() { }
	
	public float getWalkAnimSpeed() {
		if (properties.containsKey("walkanimspeed")) {
			return Float.parseFloat(properties.get("walkanimspeed").toString());
		}
		return DEFAULT_WALK_ANIM_SPEED;
	}
	
	public float getAttackAnimSpeed() {
		if (properties.containsKey("attackanimspeed")) {
			return Float.parseFloat(properties.get("attackanimspeed").toString());
		}
		return DEFAULT_ATTACK_ANIM_SPEED;
	}
	
	public float getMagicAnimSpeed() {
		if (properties.containsKey("magicanimspeed")) {
			return Float.parseFloat(properties.get("magicanimspeed").toString());
		}
		return DEFAULT_MAGIC_ANIM_SPEED;
	}
}
