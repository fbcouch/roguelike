/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * @author jami
 *
 */
public class CircleDamageBox extends DamageBox {

	/**
	 * @param world
	 * @param owner
	 * @param size
	 * @param damage
	 * @param duration
	 * @param offset
	 * @param speed
	 */
	public CircleDamageBox(World world, Character owner, Vector2 size,
			int damage, String damageType, float duration, Vector2 offset, Vector2 speed) {
		super(world, owner, size, damage, damageType, duration, offset, speed);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param world
	 * @param owner
	 * @param size
	 * @param damage
	 * @param duration
	 * @param offset
	 * @param speed
	 * @param destroyOnHit
	 */
	public CircleDamageBox(World world, Character owner, Vector2 size,
			int damage, String damageType, float duration, Vector2 offset, Vector2 speed,
			boolean destroyOnHit) {
		super(world, owner, size, damage, damageType, duration, offset, speed, destroyOnHit);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param world
	 * @param owner
	 * @param image
	 * @param damage
	 * @param duration
	 * @param offset
	 * @param speed
	 */
	public CircleDamageBox(World world, Character owner, TextureRegion image,
			int damage, String damageType, float duration, Vector2 offset, Vector2 speed) {
		super(world, owner, image, damage, damageType, duration, offset, speed);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param world
	 * @param owner
	 * @param image
	 * @param damage
	 * @param duration
	 * @param offset
	 * @param speed
	 * @param destroyOnHit
	 */
	public CircleDamageBox(World world, Character owner, TextureRegion image,
			int damage, String damageType, float duration, Vector2 offset, Vector2 speed,
			boolean destroyOnHit) {
		super(world, owner, image, damage, damageType, duration, offset, speed,
				destroyOnHit);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.DamageBox#initPhysics()
	 */
	@Override
	protected void initPhysics() {
		// Dynamic Body
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		//bodyDef.position.set(100, 100);
		
		physicsBody = world.createBody(bodyDef);
		CircleShape dynamicCircle = new CircleShape();
		dynamicCircle.setRadius(((int)(getHeight() * 0.5f) - 1) * RogueLike.WORLD_TO_BOX);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = dynamicCircle;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0;
		physicsBody.createFixture(fixtureDef).setUserData(this);
	}

	
}
