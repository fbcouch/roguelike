/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class Item extends Entity {
	public static String LOG = "Item";
	
	/**
	 * @param world
	 * @param image
	 */
	public Item(World world, EntityProto proto) {
		this(world, proto, false);
		
	}

	public Item(World world, EntityProto proto, boolean noPhysics) {
		super(world, proto, TextureManager.getSpriteFromAtlas("items.txt", proto.image, -1));
		
		if (noPhysics) {
			world.destroyBody(this.physicsBody);
			this.physicsBody = null;
		} else if (isContainer() && isStatic()) {
			if (this.physicsBody != null) world.destroyBody(this.physicsBody);
			
			// Static Body
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.StaticBody;
			
			physicsBody = world.createBody(bodyDef);
			CircleShape circleShape = new CircleShape();
			circleShape.setRadius(((int)(getHeight() * 0.5f) - 1) * RogueLike.WORLD_TO_BOX);
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = circleShape;
			fixtureDef.density = 1.0f;
			fixtureDef.friction = 0.0f;
			fixtureDef.restitution = 1;
			physicsBody.createFixture(fixtureDef).setUserData(this);
		}
	}	

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#update(float, com.ahsgaming.roguelike.entity.PlayerCharacter)
	 */
	@Override
	public void update(float delta, PlayerCharacter pc) {
		super.update(delta, pc);
		
		// for containers: need to make updates based on status
		if (isContainer()) {
			updateContainer(delta);
		}
	}
	
	//-------------------------------------------------------------------------
	// Equipment
	//-------------------------------------------------------------------------

	public int getAttackDamage() {
		if (properties.containsKey("attackdamage"))
			return (int)Float.parseFloat(properties.get("attackdamage").toString());
		return 0;
	}
	
	public float getAttackSpeed() {
		if (properties.containsKey("attackspeed"))
			return Float.parseFloat(properties.get("attackspeed").toString());
		return 0;
	}
	
	public int getAbilityPower() {
		if (properties.containsKey("abilitypower"))
			return (int)Float.parseFloat(properties.get("abilitypower").toString());
		return 0;
	}
	
	public float getCastSpeed() {
		if (properties.containsKey("castspeed"))
			return Float.parseFloat(properties.get("castspeed").toString());
		return 0;
	}
	
	public int getArmor() {
		if (properties.containsKey("armor"))
			return (int)Float.parseFloat(properties.get("armor").toString());
		return 0;
	}
	
	public String getDamageType() {
		if (properties.containsKey("damagetype"))
			return properties.get("damagetype").toString();
		return (this.id.startsWith("RUNE") ? "magic" : "physical");
	}
	
	@SuppressWarnings("unchecked")
	public float getResist(String damageType) {
		if (properties.containsKey("resists")) {
			ObjectMap<String, Object> om = (ObjectMap<String, Object>)properties.get("resists");
			if (om.containsKey(damageType)) {
				return Float.parseFloat(om.get(damageType).toString());
			}
		}
		return 0;
	}
	
	//-------------------------------------------------------------------------
	// Container stuff
	//-------------------------------------------------------------------------
	
	/**
	 * 
	 * @param delta the time since the last frame
	 */
	public void updateContainer(float delta) {
		if (getContainedItems().size == 0) {
			// container is empty
			TextureRegion emptyImage = TextureManager.getSpriteFromAtlas(
					"items.txt", this.sImage + "-open", -1);
			if (emptyImage != null)
					this.image = emptyImage;
		} else {
			// container is not empty
			TextureRegion fullImage = TextureManager.getSpriteFromAtlas(
					"items.txt", this.sImage, -1);
			
			if (fullImage == null) {
				fullImage = TextureManager.getSpriteFromAtlas("items", 
						this.sImage, -1);
			}
			
			if (fullImage != null) this.image = fullImage;
		}
	}
	
	/**
	 * 
	 * @return the boolean value of the property is-container
	 */
	public boolean isContainer() {
		if (properties.containsKey("is-container")) {
			return Boolean.parseBoolean(properties.get("is-container").toString());
		}
		return false;
	}
	
	/**
	 * 
	 * @return an Array of the item(s) contained in the contains property
	 */
	@SuppressWarnings("unchecked")
	public Array<String> getContainedItems() {
		Array<String> returnVal = new Array<String>();
		if (properties.containsKey("contains")) {
			Object o = properties.get("contains");
			if (o instanceof String) {
				returnVal.add((String)o);
			} else if (o instanceof Array) {
				returnVal.addAll((Array<String>)o);
			}
		}
		return returnVal;
	}
	
	/**
	 * 
	 * @return removes and returns all items in the contains property
	 */
	public Array<String> popContainedItems() {
		Array<String> returnVal = getContainedItems();
		if (properties.containsKey("contains")) {
			properties.put("contains", new Array<String>());
		}
		return returnVal;
	}
	
	/**
	 * 
	 * @return if the container can be picked up (ie: not treasure chests, yes for bags)
	 */
	public boolean isStatic() {
		if (properties.containsKey("static")) {
			return Boolean.parseBoolean(properties.get("static").toString());
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#shouldCollide(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public boolean shouldCollide(GameObject other) {
		if (super.shouldCollide(other)) {
			if (other instanceof PlayerCharacter) return true;
		}
		return false;
	}
}
