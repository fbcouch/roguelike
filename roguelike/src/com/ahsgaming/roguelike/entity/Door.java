/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * @author jami
 *
 */
public class Door extends Entity {
	public static String LOG = "Door";
	
	boolean locked = false;
	
	/**
	 * @param world
	 * @param image
	 */
	public Door(World world, EntityProto proto) {
		super(world, proto, TextureManager.getSpriteFromAtlas("items.txt", proto.image, -1));
		
		parseProperties();
		
		centerOffset.set(0, 0);
	}
	
	@Override
	protected void initPhysics() {
		BodyDef collideDef = new BodyDef();
		
		physicsBody = world.createBody(collideDef);
		PolygonShape tileBox = new PolygonShape();
		//EdgeShape tileBox = new EdgeShape();
		Vector2[] vertices = new Vector2[4];
		vertices[0] = (new Vector2(0, 0).mul(RogueLike.WORLD_TO_BOX));
		vertices[1] = (new Vector2(getWidth(), 0).mul(RogueLike.WORLD_TO_BOX));
		vertices[2] = (new Vector2(getWidth(), getHeight()).mul(RogueLike.WORLD_TO_BOX));
		vertices[3] = (new Vector2(0, getHeight()).mul(RogueLike.WORLD_TO_BOX));
		
		tileBox.set(vertices);
		physicsBody.createFixture(tileBox, 0.0f).setUserData(this);
		
		/*tileBox.set(vertices[0], vertices[1]);
		tileBody.createFixture(tileBox, 0.0f);
		tileBox.set(vertices[1], vertices[2]);
		tileBody.createFixture(tileBox, 0.0f);
		tileBox.set(vertices[2], vertices[3]);
		tileBody.createFixture(tileBox, 0.0f);
		tileBox.set(vertices[3], vertices[0]);
		tileBody.createFixture(tileBox, 0.0f);*/
		
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.GameObject#endContact(com.ahsgaming.roguelike.entity.GameObject)
	 */
	@Override
	public void endContact(GameObject other) {
		super.endContact(other);
		
		if (other instanceof PlayerCharacter && !locked) {
			isRemove = true;
		}
	}

	@Override
	public void parseProperties() {
		if (properties.containsKey("locked"))
			locked = Boolean.parseBoolean(properties.get("locked").toString());
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.entity.Entity#updateProperties()
	 */
	@Override
	public void updateProperties() {
		super.updateProperties();
		properties.put("locked", locked);
	}

	/**
	 * @param i
	 * @return
	 */
	public boolean canKeyUnlock(Item i) {
		return ((i.getProperties().containsKey("unlocks-all") && Boolean.parseBoolean(i.getProperties().get("unlocks-all").toString())) ||
				 (i.getProperties().containsKey("unlocks") && this.getProperties().containsKey("unlocked-by") && i.getProperties().get("unlocks").toString().equals(this.getProperties().get("unlocked-by").toString())));
	}
	
	
}
