/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.ahsgaming.roguelike.Utils;
import com.badlogic.gdx.utils.ObjectMap;

public class EntityProto {
	public String id = "";
	public String type = "";
	public String image = "";
	public ObjectMap<String, Object> properties = new ObjectMap<String, Object>();
	public String title = "";
	public String desc = "";
	
	@SuppressWarnings("unchecked")
	public EntityProto(ObjectMap<String, Object> json) {
		
		if (json.containsKey("id"))
			id = json.get("id").toString();
		
		if (json.containsKey("type"))
			type = json.get("type").toString();
		
		if (json.containsKey("image"))
			image = json.get("image").toString();
		
		if (json.containsKey("properties"))
			properties = (ObjectMap<String, Object>)json.get("properties");
		
		if (json.containsKey("title"))
			title = json.get("title").toString();
		
		if (json.containsKey("desc"))
			desc = json.get("desc").toString();
		
	}
	
	/**
	 * 
	 */
	public EntityProto() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		String json = "  {";
		json += "    " + Utils.toJsonProperty("id", id);
		json += "    " + Utils.toJsonProperty("type", type);
		json += "    " + Utils.toJsonProperty("image", image);
		json += "    " + Utils.toJsonProperty("properties", properties);
		json += "  }";
		return json;
	}
}