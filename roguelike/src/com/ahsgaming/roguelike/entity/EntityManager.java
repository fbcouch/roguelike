/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class EntityManager {
	public static String LOG = "EntityManager";
	
	static ObjectMap<String, EntityProto> prototypes;
	
	public static EntityProto get(String id) {
		if (prototypes == null) return null;
		
		if (prototypes.containsKey(id)) return prototypes.get(id);
		
		// TODO implement try-based searching to get a similar something
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static void loadEntities(String file) {
		if (prototypes == null) prototypes = new ObjectMap<String, EntityProto>();
		
		JsonReader reader = new JsonReader();
		ObjectMap<String, Object> json = (ObjectMap<String, Object>)reader.parse(Gdx.files.internal(file));
		
		if (json.containsKey("entities") && json.get("entities") instanceof Array) {
			Array<Object> jsonArray = (Array<Object>)json.get("entities");
			for (Object je: jsonArray) {
				EntityProto ep = new EntityProto((ObjectMap<String, Object>)je);
				prototypes.put(ep.id, ep);
			}
		}
	}
}
