/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.entity;

import com.badlogic.gdx.physics.box2d.World;

/**
 * @author jami
 *
 */
public class NonPlayerCharacter extends Character {

	/**
	 * @param world
	 * @param proto
	 */
	public NonPlayerCharacter(World world, EntityProto proto) {
		super(world, proto);
		// TODO Auto-generated constructor stub
	}


}
