/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike;

import java.io.IOException;
import java.io.Writer;

import com.ahsgaming.roguelike.entity.EntityManager;
import com.ahsgaming.roguelike.entity.EntityProto;
import com.ahsgaming.roguelike.entity.Item;
import com.ahsgaming.roguelike.entity.PlayerCharacter;
import com.ahsgaming.roguelike.entity.Portal;
import com.ahsgaming.roguelike.map.Map;
import com.ahsgaming.roguelike.map.Room;
import com.ahsgaming.roguelike.screens.CharacterSelectScreen;
import com.ahsgaming.roguelike.screens.LevelScreen;
import com.ahsgaming.roguelike.screens.SplashScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.ObjectMap;

public class RogueLike extends Game {
	public static String LOG = "RogueLike";
	
	public static boolean DEBUG = true;
	public static boolean DEBUG_NOSAVE = false;
	public static boolean DEBUG_QUICKSTART = false;
	public static boolean DEBUG_RENDER = true;
	
	public static final float BOX_STEP = 1/60f;
	public static final int BOX_VELOCITY_ITERATIONS = 6;
	public static final int BOX_POSITION_ITERATIONS = 2;
	public static final float WORLD_TO_BOX = 0.01f;
	public static final float BOX_TO_WORLD = 100f;
	
	private String currentPackage = "";
	public static final String EXTERNALFOLDER = ".ahsgaming/roguelike/";
	
	private FPSLogger fpsLogger = new FPSLogger();
	
	static RogueLike currentGame;
	
	int currentPlayer = -1;
	Array<Player> players = new Array<Player>();
	
    public static ObjectMap<String, Map> maps = new ObjectMap<String, Map>();
	
	String currentMap;
	
	public static ObjectMap<String, Integer> actionKeys = new ObjectMap<String, Integer>();
	
	public RogueLike() {
		currentGame = this;
	}
	
	public static RogueLike getCurrentGame() {
		return currentGame;
	}
	
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#create()
	 */
	@Override
	public void create() {
		
		
		// TODO load keymap
		actionKeys.put("move-up", Keys.UP);
		actionKeys.put("move-left", Keys.LEFT);
		actionKeys.put("move-down", Keys.DOWN);
		actionKeys.put("move-right", Keys.RIGHT);
		actionKeys.put("attack-1", Keys.SPACE);
		actionKeys.put("attack-2", Keys.SHIFT_LEFT);
		actionKeys.put("equipment", Keys.E);
		actionKeys.put("quit", Keys.Q);
		
		EntityManager.loadEntities("entities");
		load();
		
		if (DEBUG_QUICKSTART) {
			Player newPlayer = new Player();
			newPlayer.name = "Jami";
			createNewPlayer(newPlayer);
		} else {
			setSplashScreen();
		}
	}
	
	public void save() {	
		if (DEBUG_NOSAVE) return;
		if (getMap(currentMap) != null) {
			getMap(currentMap).updateMap();
			saveMaps();
		}
		
		try {
			Writer writer = Gdx.files.external(EXTERNALFOLDER + "players").writer(false);
			String json = "{\n";
			json += Utils.toJsonProperty("current", currentPlayer);
			json += Utils.toJsonProperty("players", players);
			json += "}";
			
			writer.write(json);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			Gdx.app.log(LOG, "Error saving to file: players");
		}
	}
	
	public void saveMaps() {
		if (DEBUG_NOSAVE) return;
		for(String m: getMaps().keys()) {
			saveMap(m);
		}
	}
	
	public void saveMap(String mapName) {
		if (DEBUG_NOSAVE) return;
		try {
			Writer writer = Gdx.files.external(EXTERNALFOLDER + players.get(currentPlayer).name + "." + mapName).writer(false);
			
			writer.write(getMap(mapName).toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			Gdx.app.log(LOG, "Error saving to file: " + EXTERNALFOLDER + players.get(currentPlayer).name + "." + mapName);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void load() {
		JsonReader reader = new JsonReader();
		players = new Array<Player>();
		
		if (!Gdx.files.external(EXTERNALFOLDER + "players").exists())
			Gdx.files.external(EXTERNALFOLDER + "players").writeString("{ }", false);
		
		ObjectMap<String, Object> jp = (ObjectMap<String, Object>)reader.parse(Gdx.files.external(EXTERNALFOLDER + "players"));
		
		if (jp.containsKey("current"))
			currentPlayer = (int)Float.parseFloat(jp.get("current").toString());
		
		if (jp.containsKey("players")) {
			Array<Object> pl = (Array<Object>)jp.get("players");
			for(Object o: pl) {
				players.add(new Player((ObjectMap<String, Object>)o));
			}
		}
		
		if (currentPlayer < 0 || currentPlayer >= players.size) {
			if (players.size > 0) {
				currentPlayer = 0;
				loadPlayer(players.get(currentPlayer));
			}
		} else {
			loadPlayer(players.get(currentPlayer));
		}
	}
	
	public void loadPlayer(Player p) {
		for(String m: p.maps) {
			Map map = new Map();
			map.loadFromFile(Gdx.files.external(EXTERNALFOLDER + p.name + "." + m));
			addMap(m, map);
			if (m.equals(p.currentmap)) {
				currentMap = m;
				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Game#render()
	 */
	@Override
	public void render() {
		super.render();
		
		if (DEBUG) fpsLogger.log();
	}

	public String getCurrentMap() {
		return currentMap;
	}
	
	public ObjectMap<String, Map> getMaps() {
		return maps;
	}
	
	public Map getMap(String name) {
		if (maps.size > 0 && maps.containsKey(name))
			return maps.get(name);
		return null;
	}
	
	public void addMap(String name, Map m) {
		maps.put(name, m);
	}
	
	public void removeMap(Map m) {
		String key = maps.findKey(m, true);
		if (key == null) return;
		
		maps.remove(key);
		
	}
	
	public void removeMap(String name) {
		maps.remove(name);
	}

	public void setLevelScreen() {
		setScreen(new LevelScreen(this));
	}
	
	public void setSplashScreen() {
		setScreen(new SplashScreen(this));
	}
	
	public void setCharacterSelectScreen() {
		setScreen(new CharacterSelectScreen(this));
		
	}
	
	public void setGameOverScreen() {
		// TODO implement a game over screen
		setCharacterSelectScreen();
	}
	
	public String getCurrentPackage() {
		return currentPackage;
	}
	
	public Player getCurrentPlayer() {
		if (currentPlayer < 0 || currentPlayer >= players.size  || players.size == 0) return null;
		return players.get(currentPlayer);
	}
	
	public void setCurrentPlayer(Player p) {
		currentPlayer = players.indexOf(p, true);
		loadPlayer(p);
		
		if (getMap(currentMap) == null) {
			Map map = new Map();
			map.loadFromFile(Gdx.files.internal("startingmap"));
			currentMap = "startingmap";
			addMap("startingmap", map);
			p.currentmap = "startingmap";
			p.maps.add("startingmap");
			p.x = 64;
			p.y = 64;
		}
		setScreen(new LevelScreen(this));
		save();
	}
	
	public void usePortal(Portal portal) {
		save();
		
		currentMap = portal.getMapDestination();
		
		Player pl = players.get(currentPlayer);
		pl.x = portal.getDestX();
		pl.y = portal.getDestY();
		pl.currentmap = currentMap;
		
		setScreen(new LevelScreen(this));
		save();
	}
	
	public void createNewPlayer(Player p) {
		if (this.getScreen() instanceof LevelScreen) save();
		players.add(p);
		Map map = new Map();
		map.loadFromFile(Gdx.files.internal("room-map"));
		currentMap = "room-map";
		addMap("room-map", map);
		p.currentmap = "room-map";
		p.maps.add("room-map");
		
		currentPlayer = players.indexOf(p, true);
		//loadPlayer(players.get(currentPlayer));
		p.x = 64;
		p.y = 64;
		setScreen(new LevelScreen(this));
		save();
	}
	
	public Array<Player> getPlayers() {
		return players;
	}
	
	public static class Player {
		public String name = "";
		public String currentmap = "";
		public int x = 0, y = 0;
		public Array<String> maps;
		public ObjectMap<String, Object> properties;
		public Array<EntityProto> inventory;
		public int eqSword = -1, eqRune = -1, eqArmor = -1;
		public boolean isAlive = true, isVictor = false;
		
		public Player() {
			maps = new Array<String>();
			
			inventory = new Array<EntityProto>();
			
			properties = new ObjectMap<String, Object>();
			
			properties.put("attackdamage", 5);
			properties.put("attackspeed", 0.5f);
			properties.put("abilitypower", 5);
			properties.put("armor", 0);
			properties.put("castspeed", 1f);
			properties.put("curhp", 100);
			properties.put("maxhp", 100);
			properties.put("movespeed", 2f);
			properties.put("xp", 0);
			properties.put("level", 1);
			
		}
		
		@SuppressWarnings("unchecked")
		public Player(ObjectMap<String, Object> json) {
			
			if (json.containsKey("name")) 
				name = json.get("name").toString();
			
			if (json.containsKey("isalive"))
				isAlive = Boolean.parseBoolean(json.get("isalive").toString());
			
			if (json.containsKey("isvictor"))
				isVictor = Boolean.parseBoolean(json.get("isvictor").toString());
			
			if (json.containsKey("currentmap"))
				currentmap = json.get("currentmap").toString();
			
			if (json.containsKey("x")) 
				x = (int)Float.parseFloat(json.get("x").toString());
			
			if (json.containsKey("y")) 
				y = (int)Float.parseFloat(json.get("y").toString());
			
			maps = new Array<String>();
			if (json.containsKey("maps")) {
				Array<Object> m = (Array<Object>)json.get("maps");
				for(Object o: m) {
					maps.add(o.toString());
				}
			}
			
			if (json.containsKey("properties"))
				properties = (ObjectMap<String, Object>)json.get("properties");
			
			inventory = new Array<EntityProto>();
			if (json.containsKey("inventory")) {
				Array<Object> inv = (Array<Object>)json.get("inventory");
				for (Object o: inv) {
					ObjectMap<String, Object> om = (ObjectMap<String, Object>)o;
					inventory.add(new EntityProto(om));
				}
			}
			
			if (json.containsKey("eqSword"))
				eqSword = (int)Float.parseFloat(json.get("eqSword").toString());
			
			if (json.containsKey("eqRune"))
				eqRune = (int)Float.parseFloat(json.get("eqRune").toString());
			
			if (json.containsKey("eqArmor"))
				eqArmor = (int)Float.parseFloat(json.get("eqArmor").toString());
		}
		
		@Override
		public String toString() {
			String json = "  {";
			
			json += "    " + Utils.toJsonProperty("name", name);
			json += "    " + Utils.toJsonProperty("isalive", isAlive);
			json += "    " + Utils.toJsonProperty("isvictor", isVictor);
			json += "    " + Utils.toJsonProperty("currentmap", currentmap);
			json += "    " + Utils.toJsonProperty("x", x);
			json += "    " + Utils.toJsonProperty("y", y);
			json += "    " + Utils.toJsonProperty("maps", maps);
			json += "    " + Utils.toJsonProperty("properties", properties);
			json += "    " + Utils.toJsonProperty("inventory", inventory);
			json += "    " + Utils.toJsonProperty("eqSword", eqSword);
			json += "    " + Utils.toJsonProperty("eqRune", eqRune);
			json += "    " + Utils.toJsonProperty("eqArmor", eqArmor);
			
			json += "  }";
			return json;
		}

		/**
		 * @param character
		 */
		public void update(PlayerCharacter character) {
			character.updateProperties();
			
			properties.putAll(character.getProperties());
			inventory.clear();
			for (Item i: character.getInventory()) {
				inventory.add(i.getProto());
			}
			
			if (character.getEqSword() != null) {
				eqSword = inventory.indexOf(character.getEqSword().getProto(), true);
			} else 
				eqSword = -1;
			
			if (character.getEqRune() != null) {
				eqRune = inventory.indexOf(character.getEqRune().getProto(), true);
			} else 
				eqRune = -1;
			
			if (character.getEqArmor() != null) {
				eqArmor = inventory.indexOf(character.getEqArmor().getProto(), true);
			} else 
				eqArmor = -1;
		}
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Game#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
		save();
	}
	
	
}
