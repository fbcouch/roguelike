/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.screens;

import com.ahsgaming.roguelike.RogueLike;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * @author jami
 *
 */
public class AbstractScreen implements Screen {

	final RogueLike game;
	Stage stage;
	Skin skin;
	BitmapFont fontSmall, fontMedium, fontLarge;
	
	/**
	 * 
	 */
	public AbstractScreen(RogueLike game) {
		this.game = game;
		this.stage = new Stage(0, 0, true);
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		stage.act(delta);
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		stage.draw();
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		stage.clear();
		stage.setViewport(width, height, true);
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {
		stage.clear();
		Gdx.input.setInputProcessor(stage);
		
		getSmallFont();
		getMediumFont();
		getLargeFont();
		getSkin();
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {
		dispose();
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		if (fontSmall != null) fontSmall.dispose();
		if (fontMedium != null) fontMedium.dispose();
		if (fontLarge != null) fontLarge.dispose();
		if (skin != null) skin.dispose();
	}

	public Skin getSkin() {
		if (skin == null) {
			skin = new Skin(Gdx.files.internal(game.getCurrentPackage() + "data/uiskin.json"));
		}
		return skin;
	}
	
	public BitmapFont getSmallFont() {
		if (fontSmall == null) {
			fontSmall = new BitmapFont(Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-16.fnt"),
					Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-16.png"), false);
		}
		return fontSmall;
	}
	
	public BitmapFont getMediumFont() {
		if (fontMedium == null) {
			fontMedium = new BitmapFont(Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-24.fnt"),
					Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-24.png"), false);
		}
		return fontMedium;
	}
	
	public BitmapFont getLargeFont() {
		if (fontLarge == null) {
			fontLarge = new BitmapFont(Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-32.fnt"),
					Gdx.files.internal(game.getCurrentPackage() + "fonts/kenpixel-32.png"), false);
		}
		return fontLarge;
	}
}
