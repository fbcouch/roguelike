/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.screens;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.ahsgaming.roguelike.entity.Character;
import com.ahsgaming.roguelike.entity.DamageBox;
import com.ahsgaming.roguelike.entity.Door;
import com.ahsgaming.roguelike.entity.Entity;
import com.ahsgaming.roguelike.entity.EntityManager;
import com.ahsgaming.roguelike.entity.EntityProto;
import com.ahsgaming.roguelike.entity.GameObject;
import com.ahsgaming.roguelike.entity.Item;
import com.ahsgaming.roguelike.entity.Monster;
import com.ahsgaming.roguelike.entity.PlayerCharacter;
import com.ahsgaming.roguelike.entity.Portal;
import com.ahsgaming.roguelike.map.Map;
import com.ahsgaming.roguelike.map.ObjectLayer;
import com.ahsgaming.roguelike.map.Room;
import com.ahsgaming.roguelike.map.TileLayer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactFilter;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class LevelScreen extends AbstractScreen {
	public static String LOG = "LevelScreen";
	
	static final int ITEMS_PER_ROW = 4;
	
	Group cameraGrp = new Group();
	
	
	
	Group hudGrp = new Group();
	Label nameLbl, hpLbl, attackLbl, magicLbl, armorLbl, levelLbl, xpLbl;
	
	
	PlayerCharacter character;
	
	Map map;
	
	Group entityGrp;
	
	boolean isPaused = false;
	boolean isEquipPressed = false;
	boolean isDead = false;
	float deadTimer = 3f;
	
	//---------------------------
	// Equipment screen
	Group itemGroup = new Group();
	Item selected;
	Image selector;
	Image selectedSword, selectedRune, selectedArmor;
	Label itemTitle, itemDesc;
	Group itemStats = new Group();
	
	boolean isLeftPressed = false, isRightPressed = false, isUpPressed = false, isDownPressed = false;
	boolean isAttackPressed = false, isCastPressed = false;
	
	/**
	 * @param game
	 */
	public LevelScreen(RogueLike game) {
		super(game);
	}
	

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#show()
	 */
	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
		
		// Load map
		map = game.getMap(game.getCurrentMap());
		map.init(new Vector2(game.getCurrentPlayer().x, game.getCurrentPlayer().y));
		
		entityGrp = map.getEntityGroup();
		
		ObjectMap<String, Object> charEntity = new ObjectMap<String, Object>();
		charEntity.put("id", "PLAYER");
		charEntity.put("type", game.getCurrentPlayer().name);
		charEntity.put("image", "monsters/hero.txt");
		charEntity.put("properties", game.getCurrentPlayer().properties);
		
		character = new PlayerCharacter(map.getWorld(), new EntityProto(charEntity), game.getCurrentPlayer().inventory);
		// set equipment
		if (game.getCurrentPlayer().eqSword >= 0)
			character.setEqSword(character.getInventory().get(game.getCurrentPlayer().eqSword));
		if (game.getCurrentPlayer().eqRune >= 0)
			character.setEqRune(character.getInventory().get(game.getCurrentPlayer().eqRune));
		if (game.getCurrentPlayer().eqArmor >= 0)
			character.setEqArmor(character.getInventory().get(game.getCurrentPlayer().eqArmor));
		
		
		
		//-----------------------------
		// Equipment
		selector = new Image(TextureManager.getSpriteFromAtlas("items.txt", "selector", -1));
		selectedSword = new Image(TextureManager.getSpriteFromAtlas("items.txt", "selected", -1));
		selectedRune = new Image(TextureManager.getSpriteFromAtlas("items.txt", "selected", -1));
		selectedArmor = new Image(TextureManager.getSpriteFromAtlas("items.txt", "selected", -1));
		
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		if (isPaused) {
			// add equipment screen stuff
			itemGroup.clear();
			itemGroup.addActor(selectedSword);
			itemGroup.addActor(selectedRune);
			itemGroup.addActor(selectedArmor);
			
			if (selected == null || !character.getInventory().contains(selected, true)) {
				if (character.getInventory().size > 0) selected = character.getInventory().get(0);
			}
			
			int x = 0, y = 64 * ((int)(character.getInventory().size / ITEMS_PER_ROW) + 1);
			itemGroup.setBounds(0, 0, 64 * character.getInventory().size, y);
			if (itemGroup.getWidth() > 64 * ITEMS_PER_ROW) itemGroup.setWidth(64 * ITEMS_PER_ROW);
			
			for(int i=0;i<character.getInventory().size;i++) {
				Item item = character.getInventory().get(i);
				itemGroup.addActor(item);
				item.setPosition(x - item.getCenter().x, y - item.getHeight() - item.getCenter().y);
				Gdx.app.log(LOG, String.format("Item: %d,%d (%d)", (int)item.getX(), (int)item.getY(), itemGroup.getChildren().size));
				x += 64;
				if (x >= itemGroup.getWidth()) {
					x = 0;
					y -= 64;
				}
			}
			
			itemGroup.addActor(selector);
			itemGroup.setPosition((stage.getWidth() - itemGroup.getWidth()) * 0.5f, (stage.getHeight() - itemGroup.getHeight()) * 0.5f);
			stage.addActor(itemGroup);
			
			itemTitle = new Label(" ", new LabelStyle(getMediumFont(), new Color(1, 1, 0, 1)));
			itemTitle.setPosition(itemGroup.getRight(), itemGroup.getTop() - itemTitle.getHeight());
			itemDesc = new Label(" ", new LabelStyle(getSmallFont(), new Color(1, 1, 1, 1)));
			itemDesc.setPosition(itemGroup.getRight(), itemTitle.getY() - itemDesc.getHeight());
			
			
			
			stage.addActor(itemTitle);
			stage.addActor(itemDesc);
			stage.addActor(itemStats);
			
		} else {
			cameraGrp = new Group();
			stage.addActor(cameraGrp);
			
			cameraGrp.addActor(map.getMapGroup());
			cameraGrp.addActor(entityGrp);
			
			character.setPosition(game.getCurrentPlayer().x, game.getCurrentPlayer().y);
			cameraGrp.addActor(character);
		}
		resetHUD();
		stage.addActor(hudGrp);
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if (delta > 0.019) Gdx.app.log(LOG, String.format("DeltaTime: %f", delta));
		
		if (isDead) {
			deadTimer -= delta;
			if (deadTimer <= 0) {
				game.setCharacterSelectScreen();
			}
			return;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("equipment"))) {
			if (!isEquipPressed) {
				isPaused = !isPaused;
				resize((int)stage.getWidth(), (int)stage.getHeight());
			}
			isEquipPressed = true;
		} else {
			isEquipPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("quit"))) {
			game.save();
			game.setCharacterSelectScreen();
		}
		
		if (isPaused) {
			//---------------------------
			// Equipment
			
			if(selected != null) {
				selector.setPosition(selected.getX() + selected.getCenter().x, selected.getY() + selected.getCenter().y);
				itemTitle.setText(selected.getProto().title);
				itemDesc.setText(selected.getProto().desc);
				itemStats.clear();
				setItemStats(itemStats, selected);
				itemStats.setPosition(itemGroup.getRight(), itemDesc.getY() - 10);
			}
			
			if (character.getEqSword() != null) {
				selectedSword.setVisible(true);
				selectedSword.setPosition(character.getEqSword().getX() + character.getEqSword().getCenter().x, 
						character.getEqSword().getY() + character.getEqSword().getCenter().y);
			} else {
				selectedSword.setVisible(false);
			}
			
			if (character.getEqRune() != null) {
				selectedRune.setVisible(true);
				selectedRune.setPosition(character.getEqRune().getX() + character.getEqRune().getCenter().x, 
						character.getEqRune().getY() + character.getEqRune().getCenter().y);
			} else {
				selectedRune.setVisible(false);
			}
			
			if (character.getEqArmor() != null) {
				selectedArmor.setVisible(true);
				selectedArmor.setPosition(character.getEqArmor().getX() + character.getEqArmor().getCenter().x, 
						character.getEqArmor().getY() + character.getEqArmor().getCenter().y);
			} else {
				selectedArmor.setVisible(false);
			}
			
			equipProcessInput();
			
			//---------------------------
			// Other
			
			// keep player info up to date
			game.getCurrentPlayer().update(character);
			game.getCurrentPlayer().x = (int) character.getX();
			game.getCurrentPlayer().y = (int) character.getY();
			updateHUD();
			return;
		}
		
		// check inputs
		// TODO refactor this
		character.processInput();

		//TODO need to find a better solution than this, but for whatever reason, setting cameraGrp to non-integer positions results in the tiles rendering extra pixels
		cameraGrp.setPosition((int)(-1 * character.getX() + stage.getWidth() * 0.5f), (int)(-1 * character.getY() + stage.getHeight() * 0.5f));
		
		map.update(delta, 
				stage.getCamera().combined.setToOrtho2D(-1 * cameraGrp.getX() * RogueLike.WORLD_TO_BOX, -1 * cameraGrp.getY() * RogueLike.WORLD_TO_BOX, stage.getWidth() * RogueLike.WORLD_TO_BOX, stage.getHeight() * RogueLike.WORLD_TO_BOX),
				new Vector2(character.getX(), character.getY()));
		
		// update things
		
		for (Entity e: map.getEntities()) {
			e.update(delta, character);
			if (e.isRemove()){
				map.removeEntity(e);
				if (e instanceof Character && ((Character)e).getLastDamagedBy().character.equals(character)) {
					addFloatingLabel(String.format("+%d XP", ((Character)e).getXPValue()), e.getX() + e.getCenter().x, e.getY() + e.getCenter().y);
					if (e.getProperties().containsKey("victory") && Boolean.parseBoolean(e.getProperties().get("victory").toString())) {
						// player wins
						game.getCurrentPlayer().isVictor = true;
						game.save();
						cameraGrp.addAction(Actions.fadeOut(2));
						isDead = true; // I know it isn't but this allows fade to the select screen
					}
				}
				
				if (e.getProperties().containsKey("drops")) {
					EntityProto p = EntityManager.get(e.getProperties().get("drops").toString());
					if (p != null) {
						Item i = new Item(map.getWorld(), p);
						i.setPosition(e.getX(), e.getY());
						map.addEntity(i);
					}
				}
			}
		}
		
		int curLvl = character.getLevel();
		character.update(delta, character);
		if (curLvl != character.getLevel()) {
			addFloatingLabel("LEVEL UP!", character.getX() + character.getCenter().x, character.getY() + character.getCenter().y);
		}
		game.getCurrentPlayer().update(character);
		game.getCurrentPlayer().x = (int) character.getX();
		game.getCurrentPlayer().y = (int) character.getY();
		
		if (character.getHP() <= 0) {
			// player dies
			game.getCurrentPlayer().isAlive = false;
			game.save();
			
			cameraGrp.addAction(Actions.fadeOut(2));
			
			isDead = true;
		}
				
		updateHUD();
	}
	
	/**
	 * 
	 * @param group
	 * @param item
	 */
	private void setItemStats(Group group, Item item) {
		int x = 0, y = 0;
		if (item.getProperties().containsKey("curhp")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("CUR HP: %d", (int)Float.parseFloat(item.getProperties().get("curhp").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("maxhp")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("MAX HP: %d", (int)Float.parseFloat(item.getProperties().get("maxhp").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("attackdamage")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("ATTACK DMG: %d", (int)Float.parseFloat(item.getProperties().get("attackdamage").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("attackspeed")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("ATTACK SPD: %.2f", Float.parseFloat(item.getProperties().get("attackspeed").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("abilitypower")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("MAGIC DMG: %d", (int)Float.parseFloat(item.getProperties().get("abilitypower").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("castspeed")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("CAST SPD: %.2f", Float.parseFloat(item.getProperties().get("castspeed").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
		
		if (item.getProperties().containsKey("armor")) {
			Label lbl = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lbl.setText(String.format("ARMOR: %d", (int)Float.parseFloat(item.getProperties().get("armor").toString())));
			lbl.setPosition(x, y - lbl.getHeight());
			y -= lbl.getHeight();
			group.addActor(lbl);
		}
	}


	protected void equipProcessInput() {
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-right"))) {
			if (!isRightPressed && character.getInventory().size > 0) {
				int i = character.getInventory().indexOf(selected, true);
				i = (i + 1) % character.getInventory().size;
				selected = character.getInventory().get(i);
			}
			isRightPressed = true;
		} else {
			isRightPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-left"))) {
			if (!isLeftPressed && character.getInventory().size > 0) {
				int i = character.getInventory().indexOf(selected, true);
				i = (i - 1);
				if (i < 0) 
					i = character.getInventory().size + i;
				selected = character.getInventory().get(i);
			}
			isLeftPressed = true;
		} else {
			isLeftPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-up"))) {
			if (!isUpPressed && character.getInventory().size > 0) {
				int i = character.getInventory().indexOf(selected, true);
				i = (i - ITEMS_PER_ROW);
				if (i < 0) {
					i += ITEMS_PER_ROW;
					i = ((int)((character.getInventory().size - 1) / ITEMS_PER_ROW)) * ITEMS_PER_ROW + i;
					if (i >= character.getInventory().size) {
						i -= ITEMS_PER_ROW;
					}
				}
				selected = character.getInventory().get(i);
			}
			isUpPressed = true;
		} else {
			isUpPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-down"))) {
			if (!isDownPressed && character.getInventory().size > 0) {
				int i = character.getInventory().indexOf(selected, true);
				i = (i + ITEMS_PER_ROW);
				if (i >= character.getInventory().size) 
					i %= ITEMS_PER_ROW;
				selected = character.getInventory().get(i);
			}
			isDownPressed = true;
		} else {
			isDownPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("attack-1"))) {
			if (!isAttackPressed && selected != null) {
				if (selected.getType().equals("sword")) {
					if (character.getEqSword() == selected) {
						character.setEqSword(null);
					} else {
						character.setEqSword(selected);
					}
				} else if (selected.getType().equals("rune")) {
					if (character.getEqRune() == selected) {
						character.setEqRune(null);
					} else {
						character.setEqRune(selected);
					}
				} else if (selected.getType().equals("armor")) {
					if (character.getEqArmor() == selected) {
						character.setEqArmor(null);
					} else {
						character.setEqArmor(selected);
					}
				} else if (selected.getType().equals("usable")) {
					character.useItem(selected);
					resize((int)stage.getWidth(), (int)stage.getHeight());
				}
			}
			isAttackPressed = true;
		} else {
			isAttackPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("attack-2"))) {
			if (!isCastPressed && selected != null) {
				// destroy the current item
				// TODO add confirmation?
				character.removeItem(selected);
				resize((int)stage.getWidth(), (int)stage.getHeight());
			}
			isCastPressed = true;
		} else {
			isCastPressed = false;
		}
	}
	
	protected void resetHUD() {
		hudGrp.clear();
		
		nameLbl = new Label(game.getCurrentPlayer().name, new LabelStyle(getMediumFont(), new Color(1,1,1,1)));
		nameLbl.setPosition(25, stage.getHeight() - nameLbl.getHeight());
		hpLbl = new Label("HP", new LabelStyle(getMediumFont(), new Color(1,1,1,1)));
		hpLbl.setPosition(nameLbl.getX(), nameLbl.getY() - nameLbl.getHeight());
		
		attackLbl = new Label("ATTACK", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		attackLbl.setPosition(hpLbl.getX(), hpLbl.getY() - attackLbl.getHeight() * 2);
		magicLbl = new Label("MAGIC", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		magicLbl.setPosition(attackLbl.getX(), attackLbl.getY() - magicLbl.getHeight());
		armorLbl = new Label("ARMOR", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		armorLbl.setPosition(magicLbl.getX(), magicLbl.getY() - armorLbl.getHeight());
		
		levelLbl = new Label("LEVEL", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		levelLbl.setPosition(armorLbl.getX(), armorLbl.getY() - levelLbl.getHeight() * 2);
		xpLbl = new Label("XP", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		xpLbl.setPosition(levelLbl.getX(), levelLbl.getY() - xpLbl.getHeight());
		
		hudGrp.addActor(nameLbl);
		hudGrp.addActor(hpLbl);
		hudGrp.addActor(attackLbl);
		hudGrp.addActor(magicLbl);
		hudGrp.addActor(armorLbl);
		hudGrp.addActor(levelLbl);
		hudGrp.addActor(xpLbl);
	}
	
	protected void updateHUD() {
		hpLbl.setText(String.format("HP: %d/%d", character.getHP(), character.getMaxHP()));
		attackLbl.setText(String.format("ATTACK: %d (%.2f)", character.getAttackDamage(), character.getAttackSpeed()));
		magicLbl.setText(String.format("MAGIC: %d (%.2f)", character.getAbilityPower(), character.getCastSpeed()));
		armorLbl.setText(String.format("ARMOR: %d", character.getArmor()));
		
		levelLbl.setText(String.format("LEVEL: %d", character.getLevel()));
		xpLbl.setText(String.format("XP TO NEXT: %d", character.getXPforLevel(character.getLevel()) - character.getXP()));
	}
	
	public void addFloatingLabel(String text, float x, float y) {
		Gdx.app.log(LOG, "Floating Label!");
		Label lbl = new Label(text, new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		lbl.setPosition(x, y);
		lbl.addAction(Actions.parallel(Actions.fadeOut(1f), Actions.moveBy(0, 64f, 1f)));
		cameraGrp.addActor(lbl);
	}
}
