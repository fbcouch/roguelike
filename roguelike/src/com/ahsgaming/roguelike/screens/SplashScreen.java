/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.screens;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.TextureManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * @author jami
 *
 */
public class SplashScreen extends AbstractScreen {

	Image splash;
	float countdown = 3;
	
	/**
	 * @param game
	 */
	public SplashScreen(RogueLike game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);
		countdown -= delta;
	
		if (Gdx.input.isKeyPressed(Keys.ESCAPE) || Gdx.input.isKeyPressed(Keys.SPACE) || Gdx.input.isKeyPressed(Keys.ENTER)) 
			countdown = 0;
		
		if (countdown <= 0) {
			game.setCharacterSelectScreen();
		}
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		splash.setPosition((width - splash.getWidth()) * 0.5f, (height - splash.getHeight()) * 0.5f);
		stage.addActor(splash);
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#show()
	 */
	@Override
	public void show() {
		super.show();
		splash = new Image(TextureManager.getTexture("splash.png"));
		splash.setColor(1,1,1,0);
		splash.addAction(Actions.sequence(Actions.fadeIn(1.0f), Actions.delay(1.0f), Actions.fadeOut(1.0f)));
		
	}

	
}
