/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike.screens;

import com.ahsgaming.roguelike.RogueLike;
import com.ahsgaming.roguelike.RogueLike.Player;
import com.ahsgaming.roguelike.TextureManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class CharacterSelectScreen extends AbstractScreen {
	public static String LOG = "CharacterSelectScreen";
	
	Player selected;
	Image selector;
	ObjectMap<Player, Image> playerImages = new ObjectMap<Player, Image>();
	Group playerGrp;
	
	boolean isUpPressed = false, isDownPressed = false, isEnterPressed = false, isEscPressed = true;
	TextField txtName;
	Label lblNewInstr;
	
	Player newPlayer;
	
	Label instructions;
	
	/**
	 * @param game
	 */
	public CharacterSelectScreen(RogueLike game) {
		super(game);
	}
	
	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#show()
	 */
	@Override
	public void show() {
		super.show();
		
		selected = game.getCurrentPlayer();
		
		selector = new Image(TextureManager.getSpriteFromAtlas("items.txt", "selector", -1));
		
		instructions = new Label("PRESS ENTER TO SELECT", new LabelStyle(getMediumFont(), new Color(1,1,1,1)));
		
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		playerImages.clear();
		playerGrp = new Group();
		Image sel = null;
		int y = 0;
		
		for (Player p: game.getPlayers()) {
			Image img = new Image(TextureManager.getSpriteFromAtlas("monsters/hero.txt", "walk-s", 0));
			img.setPosition(0, y);
			if (p == game.getCurrentPlayer() || sel == null) {
				sel = img;
				
			}
			
			if (selected == null) {
				selected = p;
			}
			playerGrp.addActor(img);
			playerImages.put(p, img);
			
			Label lblName = new Label(p.name, new LabelStyle(getMediumFont(), new Color(1, 1, 0, 1)));
			lblName.setPosition(img.getRight() + 10, img.getTop() - lblName.getHeight());
			playerGrp.addActor(lblName);
			
			int level = 1;
			if (p.properties.containsKey("level"))
				level = (int)Float.parseFloat(p.properties.get("level").toString());
			
			Label lblStatus = new Label(
					(p.isAlive ? 
							(p.isVictor ? 
									String.format("VICTOR (LVL %d)", level) : 
									String.format("LVL %d", level)) : 
							String.format("DEAD (LVL %d)", level)), 
					new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
			lblStatus.setPosition(lblName.getX(), lblName.getY() - lblStatus.getHeight());
			playerGrp.addActor(lblStatus);
			
			y += img.getHeight();
			playerGrp.setHeight(img.getTop());
			if (lblName.getRight() > playerGrp.getWidth()) playerGrp.setWidth(lblName.getRight());
		}
		
		newPlayer = new Player();
		Image img = new Image(TextureManager.getSpriteFromAtlas("monsters/hero.txt", "walk-s", 0));
		img.setPosition(0, y);
		if (game.getPlayers().size == 0) {
			sel = img;
			selected = newPlayer;
		}
		playerGrp.addActor(img);
		playerImages.put(newPlayer, img);
		
		txtName = new TextField("New Player", new TextFieldStyle(getMediumFont(), new Color(1,1,1,1), new TextureRegionDrawable(TextureManager.getTexture("cursor.png")), null, null));
		txtName.setTextFieldFilter(new TextFieldFilter() {

			@Override
			public boolean acceptChar(TextField textField, char key) {
				// TODO Auto-generated method stub
				return Character.isLetterOrDigit(key) || key == '_' || key == '-';
			}
			
		});
		txtName.setPosition(img.getRight() + 10, img.getTop() - txtName.getHeight());
		txtName.setWidth(200);
		playerGrp.addActor(txtName);
		
		lblNewInstr = new Label(" ", new LabelStyle(getSmallFont(), new Color(1,1,1,1)));
		lblNewInstr.setPosition(txtName.getX(), txtName.getY() - lblNewInstr.getHeight());
		playerGrp.addActor(lblNewInstr);
		
		playerGrp.setHeight(img.getTop());
		if (txtName.getRight() > playerGrp.getWidth()) playerGrp.setWidth(txtName.getRight());
		
		stage.addActor(playerGrp);
		playerGrp.setPosition((width - playerGrp.getWidth()) * 0.5f, sel.getY() + (height - sel.getHeight()) * 0.5f);
		stage.addActor(selector);
		
		stage.addActor(instructions);
		instructions.setPosition(playerGrp.getX() - instructions.getWidth() - 10, (stage.getHeight() - instructions.getHeight()) * 0.5f);
	}

	/* (non-Javadoc)
	 * @see com.ahsgaming.roguelike.screens.AbstractScreen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if (selected != null) {
			Image sel = playerImages.get(selected);
			playerGrp.setPosition((stage.getWidth() - playerGrp.getWidth()) * 0.5f, (stage.getHeight() - sel.getHeight()) * 0.5f - sel.getY());
			selector.setPosition(sel.getParent().getX() + sel.getX(), sel.getParent().getY() + sel.getY());
			
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-up"))) {
			if (!isUpPressed) {
				int i = game.getPlayers().indexOf(selected, true);
				if (i == -1) {
					selected = game.getPlayers().first();
				} else {
					i += 1;
					if (i >= game.getPlayers().size){
						selected = newPlayer;
					} else {
						selected = game.getPlayers().get(i);
					}
				}
				
			}
			isUpPressed = true;
			
		} else {
			isUpPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(RogueLike.actionKeys.get("move-down"))) {
			if (!isDownPressed) {
				int i = game.getPlayers().indexOf(selected, true);
				if (i == -1) {
					selected = game.getPlayers().get(game.getPlayers().size - 1);
				} else {
					i -= 1;
					if (i < 0){
						selected = newPlayer;
					} else {
						selected = game.getPlayers().get(i);
					}
				}
				
			}
			isDownPressed = true;
		} else {
			isDownPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(Keys.ENTER)) {
			if (!isEnterPressed) {
				int i = game.getPlayers().indexOf(selected, true);
				if (i == -1) {
					stage.setKeyboardFocus(txtName);
					if (!(txtName.getText().equals("New Player") || txtName.getText().equals(""))) {
						boolean found = false;
						for (Player p: game.getPlayers()) {
							if (p.name.equals(txtName.getText()) && p.isAlive && !p.isVictor) found = true;
						}
						
						if (found) {
							lblNewInstr.setText("NOT AVAILABLE");
						} else {
							newPlayer.name = txtName.getText();
							game.createNewPlayer(newPlayer);
						}
						
					} else if (txtName.getText().equals("New Player")) {
						txtName.setText("");
						lblNewInstr.setText("TYPE YOUR NAME");
					}
				} else {
					if (selected.isAlive && !selected.isVictor) {
						game.setCurrentPlayer(selected);
					}
					
				}
			}
			isEnterPressed = true;
		} else {
			isEnterPressed = false;
		}
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			if (!isEscPressed) Gdx.app.exit();
			isEscPressed = true;
		} else {
			isEscPressed = false;
		}
	}

}
