/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class TextureManager {
	public static String LOG = "TextureManager";
	
	private static ObjectMap<String, TextureRegion> map = new ObjectMap<String, TextureRegion>(); 
	
	public static TextureRegion getTexture(String file) {
		if (map.containsKey(file)) return map.get(file);
		
		
		Texture tex = new Texture(Gdx.files.internal(file));
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		TextureRegion reg = new TextureRegion(tex); 
		
		map.put(file, reg);
		return reg;
	}
	
	public static void loadTexture(String file) {
		getTexture(file);
	}
	
	public static TextureRegion loadTextureRegion(String file, int x, int y, int w, int h) {
		return new TextureRegion(getTexture(file), x, y, w, h);
	}
	
	private static ObjectMap<String, TextureAtlas> atlases = new ObjectMap<String, TextureAtlas>();
	
	public static Sprite getSpriteFromAtlas(String atlas, String name, int id) {
		if (!atlases.containsKey(atlas))
			atlases.put(atlas, new TextureAtlas(atlas));
		
		if (id == -1)
			return atlases.get(atlas).createSprite(name);
		
		return atlases.get(atlas).createSprite(name, id);
	}
	
	public static Array<Sprite> getSpritesFromAtlas(String atlas, String name) {
		if (!atlases.containsKey(atlas))
			atlases.put(atlas, new TextureAtlas(atlas));
			
		return atlases.get(atlas).createSprites(name);
	}
	
	public static void loadTexturePackage(String name) {
		map.clear();
		JsonReader jsonReader = new JsonReader();
		Object rObj = jsonReader.parse(Gdx.files.internal(name + "/package.json"));
		ObjectMap<String, Object> mapObjs = (ObjectMap<String, Object>)rObj;
		for (String key : mapObjs.keys()) {
			ObjectMap<String, Object> createObj = (ObjectMap<String, Object>)mapObjs.get(key);
			String file = "";
			int x = 0, y = 0, w = 0, h = 0;
			
			if (createObj.containsKey("file")) {
				file = name + "/" + createObj.get("file").toString();
			}
			
			if (createObj.containsKey("x")) {
				x = (int)Float.parseFloat(createObj.get("x").toString());
			}
			
			if (createObj.containsKey("y")) {
				y = (int)Float.parseFloat(createObj.get("y").toString());
			}
			
			if (createObj.containsKey("w")) {
				w = (int)Float.parseFloat(createObj.get("w").toString());
			}
			
			if (createObj.containsKey("h")) {
				h = (int)Float.parseFloat(createObj.get("h").toString());
			}
			Gdx.app.log(LOG, String.format("Loading %s from %s", key, file));
			map.put(key, loadTextureRegion(file, x, y, w, h));
		}
	}

	
}
