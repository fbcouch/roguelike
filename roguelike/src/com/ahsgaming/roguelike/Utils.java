/**
 * Legend of Rogue
 * An AHS Gaming Production
 * (c) 2013 Jami Couch
 * fbcouch 'at' gmail 'dot' com
 * Licensed under Apache 2.0
 * See www.ahsgaming.com for more info
 * 
 * LibGDX
 * (c) 2011 see LibGDX authors file
 * Licensed under Apache 2.0
 * 
 * Pixelated Fonts by Kenney, Inc. Licensed as CC-SA.
 * See http://kenney.nl for more info.
 * 
 * All other art assets (c) 2013 Jami Couch, licensed CC-BY-SA
 */
package com.ahsgaming.roguelike;

import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author jami
 *
 */
public class Utils {
	public static final String LOG = "Utils";
	
	public static String toJsonProperty(String key, Object value) {
		String valueStr = value.toString();
		if (value instanceof ObjectMap) {
			valueStr = "\n{\n ";
			ObjectMap<String, Object> om = (ObjectMap<String, Object>)value;
			for (String k: om.keys()) {
				valueStr += "  " + toJsonProperty(k, om.get(k));
			}
			valueStr += " }";
		}
		return "\"" + key + "\": " + valueStr + ",\n";
	}
}
